# endian-type-rs

A fork of [endian-type](https://crates.io/crates/endian-type) which includes support for 
no-std and the [From](https://doc.rust-lang.org/std/convert/trait.From.html) trait.

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies.endian-type-rs]
version = "0.1.4"
```

and this to your crate root:

```rust
extern crate endian_type_rs;
```

## Features

This crate can be used without the standard library (`#![no_std]`) by disabling
the default `std` feature. Use this in `Cargo.toml`:

```toml
[dependencies.endian-type-rs]
version = "0.1.4"
default-features = false
# features = ["num"]    # <--- Uncomment if you wish to include support for num::ToPrimitive
```

The `"num"` feature will add additional definitions for simplified casting via
the num crate "num".

Previously casting between the internal types and another built-in
numeric type required a decent amount of boilerplate. In the worst case
you needed to specify both the current Big/LittleEndian's underlying
type as well as the type you were trying to cast to. E.g.

    let val: u32_be = u32_be::from(10);
    let addr = u32::from(val) as usize;

This isn't much overhead here where the type is obvious, however if you
change the underlying type of "val" in the above example, you must
change every single cast. Now the above becomes:

    let val: u32_be = u32_be::from(10);
    let addr = val.to_usize().unwrap();
